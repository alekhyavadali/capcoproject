(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table ,tr td{\n    border:1px solid;\n}\ntbody {\n    display:block;\n    height:600px;\n    overflow-x: scroll;\n    overflow-y: scroll;\n}\nthead, tbody tr {\n    display:table;\n     width:2000px;\n    table-layout:fixed;\n    word-wrap: break-word;\n}\nthead {\n    width: 100%;\n    height: 30px;\n    display: inline-table;\n    overflow-x: scroll;\n    \n}\ntable {\n    width:100%;\n    display: table-header-group;\n    height:600px;\n    overflow-x: scroll;\n    overflow-y: scroll;\n}\ntr:hover {background-color: #f5f5f5;}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    // VARIABLES
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./data.service */ "./src/app/data.service.ts");
/* harmony import */ var _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pagination/pagination.component */ "./src/app/pagination/pagination.component.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _keys_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./keys.pipe */ "./src/app/keys.pipe.ts");
/* harmony import */ var _credit_credit_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./credit/credit.component */ "./src/app/credit/credit.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// Components



// HTTP


// Routes



var appRoutes = [
    { path: '', component: _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_4__["PaginationComponent"] },
    { path: 'credit', component: _credit_credit_component__WEBPACK_IMPORTED_MODULE_9__["CreditComponent"] },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_4__["PaginationComponent"],
                _credit_credit_component__WEBPACK_IMPORTED_MODULE_9__["CreditComponent"],
                _keys_pipe__WEBPACK_IMPORTED_MODULE_8__["KeysPipe"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_5__["HttpModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forRoot(appRoutes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"]],
            providers: [_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/credit/credit.component.css":
/*!*********************************************!*\
  !*** ./src/app/credit/credit.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table ,tr td{\n    border:1px solid  burlywood;\n}\ntbody {\n    display:block;\n    height:600px;\n    overflow-x: scroll;\n    overflow-y: scroll;\n}\nthead, tbody tr {\n    display:table;\n     width:2000px;\n    table-layout:fixed;\n    word-wrap: break-word;\n}\nthead {\n    width: 100%;\n    height: 30px;\n    display: inline-table;\n    overflow-x: scroll;\n    \n}\ntable {\n    width:100%;\n    display: table-header-group;\n    height:600px;\n    overflow-x: scroll;\n    overflow-y: scroll;\n}\ntr:hover {background-color: #f5f5f5;}\n.mainHeading {\n    text-align: center;\n    color: #BC9B5D;\n}\n"

/***/ }),

/***/ "./src/app/credit/credit.component.html":
/*!**********************************************!*\
  !*** ./src/app/credit/credit.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"mainHeading\">\n  <!-- Main Heading -title -->\n  <h1>Capco extra credit Table</h1>\n</div>\n<div>\n  <!-- Link to the extra Pagination table -->\n  <button (click)=\"goToPaginationTable()\"> Link to main Table</button><br>\n  <div>\n    Select Number of rows: <select (change)=changeLengthOfData($event.target.value)>\n      <option></option>\n      <option value=\"5\">5</option>\n      <option value=\"15\">15</option>\n      <option value=\"25\">25</option>\n      <option value=\"50\">50</option>\n      <option value=\"100\">100</option>\n    </select>\n  </div>\n  <!-- Required representation of the table -->\n  <table>\n    <!-- Header of the table -->\n    <thead>\n      <tr>\n        <th *ngFor=\"let item of paginatedData[0] | keys\">{{item}}</th>\n      </tr>\n    </thead>\n    <!-- Body of the table -->\n    <tbody>\n      <tr *ngFor=\"let value of paginatedData\">\n        <td *ngFor=\"let key of value | keys\">\n          {{value[key]}}\n        </td>\n      </tr>\n      <p>Developed by Alekhya Vadali</p>\n    </tbody>\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/credit/credit.component.ts":
/*!********************************************!*\
  !*** ./src/app/credit/credit.component.ts ***!
  \********************************************/
/*! exports provided: CreditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreditComponent", function() { return CreditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CreditComponent = /** @class */ (function () {
    /**
    * Constructor. Here we will get the required the json format from the Sample_data.json
     */
    function CreditComponent(_dataService, router) {
        var _this = this;
        this._dataService = _dataService;
        this.router = router;
        // VARIABLES
        this.sampleData = [];
        this.options = [5, 15, 25, 50];
        this.paginatedData = [];
        this._dataService.readDataFromJson()
            .subscribe(function (res) {
            _this.sampleData = res;
            _this.paginatedData = _this.sampleData;
            console.log(_this.sampleData);
        });
    }
    CreditComponent.prototype.ngOnInit = function () {
    };
    /**
     * This method will route to the Pagination component
     */
    CreditComponent.prototype.goToPaginationTable = function () {
        this.router.navigateByUrl('');
    };
    /**
    * By default all the rows are displayed. On clicking number of rows this method
    * is called. It will displayed only number of rows selected by user.
    */
    CreditComponent.prototype.changeLengthOfData = function (option) {
        this.paginatedData = [];
        this.optionSelected = (parseInt(option, 10));
        if (this.optionSelected != null) {
            for (var i = 0; i < this.optionSelected; i++) {
                this.paginatedData.push(this.sampleData[i]);
            }
        }
    };
    CreditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-credit',
            template: __webpack_require__(/*! ./credit.component.html */ "./src/app/credit/credit.component.html"),
            styles: [__webpack_require__(/*! ./credit.component.css */ "./src/app/credit/credit.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], CreditComponent);
    return CreditComponent;
}());



/***/ }),

/***/ "./src/app/data.service.ts":
/*!*********************************!*\
  !*** ./src/app/data.service.ts ***!
  \*********************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DataService = /** @class */ (function () {
    function DataService(_http) {
        this._http = _http;
    }
    /**
       * Reads the content of the file sample_data.json and returns the promise object
       */
    DataService.prototype.readDataFromJson = function () {
        var fileName = '../assets/sample_data.json';
        return this._http.get(fileName);
    };
    DataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "./src/app/keys.pipe.ts":
/*!******************************!*\
  !*** ./src/app/keys.pipe.ts ***!
  \******************************/
/*! exports provided: KeysPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KeysPipe", function() { return KeysPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var KeysPipe = /** @class */ (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (value, args) {
        var keys = [];
        for (var key in value) {
            if (true) {
                keys.push(key);
            }
        }
        return keys;
    };
    KeysPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'keys' })
    ], KeysPipe);
    return KeysPipe;
}());



/***/ }),

/***/ "./src/app/pagination/pagination.component.css":
/*!*****************************************************!*\
  !*** ./src/app/pagination/pagination.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table ,tr td{\n    border:1px solid burlywood;\n}\n.mainHeading {\n    text-align: center;\n    color: #BC9B5D;\n}\ntbody {\n    display:block;\n    height:600px;\n    overflow-x: scroll;\n    overflow-y: scroll;\n}\nthead, tbody tr {\n    display:table;\n     width:2000px;\n    table-layout:fixed;\n    word-wrap: break-word;\n}\nthead {\n    width: 100%;\n    height: 30px;\n    display: inline-table;\n    overflow-x: scroll;\n    \n}\ntable {\n    width:100%;\n    display: table-header-group;\n    height:600px;\n    overflow-x: scroll;\n    overflow-y: scroll;\n}\ntr:hover {background-color: #f5f5f5;}\n.pagination button {\n    color: black;\n    float: left;\n    padding: 8px 16px;\n    text-decoration: none;\n    transition: background-color .3s;\n  }\n.pagination button.active {\n    background-color: dodgerblue;\n    color: white;\n  }\n.pagination button:hover:not(.active) {background-color: #ddd;}\n  "

/***/ }),

/***/ "./src/app/pagination/pagination.component.html":
/*!******************************************************!*\
  !*** ./src/app/pagination/pagination.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"mainHeading\">\n  <!-- Main Heading -title -->\n  <h1>Capco Main Table</h1>\n\n</div>\n<div>\n  <!-- Link to the extra credit table -->\n  <button (click)=\"gotoCreditPage()\"> Link to extra Credit Table</button><br>\n  <div>\n    <br>\n    <!-- Select the number of rows to start pagination -->\n    Select Number of rows(select to get pagination): <select (change)=changeLengthOfData($event.target.value)>\n      <option></option>\n      <option value=\"10\">10</option>\n      <option value=\"15\">15</option>\n      <option value=\"25\">25</option>\n      <option value=\"50\">50</option>\n    </select>\n  </div>\n  <!-- Required representation of the table -->\n  <table>\n    <thead>\n      <tr>\n        <!-- Header of the table -->\n        <th *ngFor=\"let item of paginatedData[0] | keys\">{{item}}</th>\n        <th>Submit</th>\n      </tr>\n    </thead>\n    <!-- Body of the table -->\n    <tbody>\n      <tr *ngFor=\"let value of paginatedData\">\n        <td *ngFor=\"let key of value | keys\">\n          {{value[key]}}\n        </td>\n        <td><button (click) = submitToServer(value.id,value.status)>send</button></td>\n      </tr>\n    </tbody>\n    <!-- Pagination will start from here -->\n    <tr>\n      <div class=\"pagination\">\n\n        <button *ngFor=\"let num of numbersArray; let index = i\" (click)=\"clickPagination(num);\">\n          {{num}}</button>\n\n      </div>\n    </tr>\n    <p>Developed by Alekhya Vadali</p>\n  </table>\n\n</div>"

/***/ }),

/***/ "./src/app/pagination/pagination.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pagination/pagination.component.ts ***!
  \****************************************************/
/*! exports provided: PaginationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationComponent", function() { return PaginationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PaginationComponent = /** @class */ (function () {
    /**
     * Constructor. Here we will get the required the json format from the Sample_data.json
      */
    function PaginationComponent(_dataService, router, http) {
        var _this = this;
        this._dataService = _dataService;
        this.router = router;
        this.http = http;
        // VARIABLES
        this.sampleData = [];
        this.options = [5, 15, 25, 50];
        this.optionSelected = 5;
        this.paginatedData = [];
        this.numbersArray = [];
        this._dataService.readDataFromJson()
            .subscribe(function (res) {
            _this.sampleData = res;
            _this.paginatedData = _this.sampleData;
            console.log(_this.sampleData);
        });
    }
    PaginationComponent.prototype.ngOnInit = function () {
    };
    /**
        * By default all the rows are displayed. On clicking number of rows this method
        * is called. It will displayed only number of rows selected by user in each pagination
        */
    PaginationComponent.prototype.changeLengthOfData = function (option) {
        this.paginatedData = [];
        this.numbersArray = [];
        this.optionSelected = (parseInt(option, 10));
        if (this.optionSelected != null) {
            for (var i = 0; i < this.optionSelected; i++) {
                this.paginatedData.push(this.sampleData[i]);
            }
        }
        if ((this.sampleData.length) % this.optionSelected !== 0) {
            this.numOfPages = (this.sampleData.length / this.optionSelected) + 1;
        }
        else {
            this.numOfPages = Math.round(this.sampleData.length / this.optionSelected);
        }
        for (var i = 1; i <= this.numOfPages; i++) {
            this.numbersArray.push(i);
        }
    };
    /**
      * Onclick pagination, the set of user selected rows selected by user will be displayed in each page.
      */
    PaginationComponent.prototype.clickPagination = function (paginatedValue) {
        this.paginatedData = [];
        var lowerLimit = (this.optionSelected * (paginatedValue - 1));
        var upperLimit = (this.optionSelected * (paginatedValue));
        if (this.sampleData.length < upperLimit) {
            upperLimit = this.sampleData.length;
        }
        for (var i = lowerLimit; i < upperLimit; i++) {
            this.paginatedData.push(this.sampleData[i]);
        }
    };
    /**
      * This method will route to the Credit component
      */
    PaginationComponent.prototype.gotoCreditPage = function () {
        this.router.navigateByUrl('credit');
    };
    PaginationComponent.prototype.submitToServer = function (id, status) {
        var data = { id: id, status: status };
        return this.http.post('/api/submit', data).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (res) {
            console.log(id, status);
        }));
    };
    PaginationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pagination',
            template: __webpack_require__(/*! ./pagination.component.html */ "./src/app/pagination/pagination.component.html"),
            styles: [__webpack_require__(/*! ./pagination.component.css */ "./src/app/pagination/pagination.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], PaginationComponent);
    return PaginationComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/alekhyavadali/Documents/capcoproject/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map