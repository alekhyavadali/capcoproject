import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditComponent } from './credit.component';
import { KeysPipe } from '../keys.pipe';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Router } from '@angular/router';
describe('CreditComponent', () => {
  let component: CreditComponent;
  let fixture: ComponentFixture<CreditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditComponent, KeysPipe ],
      providers: [HttpClient, HttpHandler, {
        provide: Router,
        useClass: class { navigate = jasmine.createSpy('navigate'); }}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
