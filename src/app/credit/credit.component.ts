import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {
  // VARIABLES
  sampleData = [];
  options = [5, 15, 25, 50];
  optionSelected: number;
  paginatedData = [];

  /**
  * Constructor. Here we will get the required the json format from the Sample_data.json
   */

  constructor(private _dataService: DataService, private router: Router) {
    this._dataService.readDataFromJson()
      .subscribe(res => {
        this.sampleData = res;
        this.paginatedData = this.sampleData;
        console.log(this.sampleData);
      });
  }

  ngOnInit() {

  }
  /**
   * This method will route to the Pagination component
   */

  goToPaginationTable() {
    this.router.navigateByUrl('');
  }

  /**
  * By default all the rows are displayed. On clicking number of rows this method
  * is called. It will displayed only number of rows selected by user.
  */

  changeLengthOfData(option: string) {
    this.paginatedData = [];
    this.optionSelected = (parseInt(option, 10));
    if (this.optionSelected != null) {
      for (let i = 0; i < this.optionSelected; i++) {
        this.paginatedData.push(this.sampleData[i]);
      }
    }
  }

}
