import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterModule, Routes, Router, ChildrenOutletContexts } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { HttpInterceptorHandler } from '@angular/common/http/src/interceptor';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
      ],
      imports: [RouterModule],
      providers: [ {
        provide: Router,
        useClass: class { navigate = jasmine.createSpy('navigate'); }}, ChildrenOutletContexts]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
describe ('unittest', () => {
  it('checks if hellotest is hellotest', () => expect('hellotest').toBe('hellotest'));
  });
