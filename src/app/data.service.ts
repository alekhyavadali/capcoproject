import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(public _http: HttpClient) { }
  /**
     * Reads the content of the file sample_data.json and returns the promise object
     */
  public readDataFromJson(): Observable<any> {
    const fileName = '../assets/sample_data.json';
    return this._http.get(fileName);
  }
}
