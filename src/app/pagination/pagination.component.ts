import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { RouterModule, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  // VARIABLES
  sampleData = [];
  options = [5, 15, 25, 50];
  optionSelected = 5;
  paginatedData = [];
  numOfPages: number;
  numbersArray = [];

  /**
   * Constructor. Here we will get the required the json format from the Sample_data.json
    */

  constructor(private _dataService: DataService, private router: Router, private http: HttpClient) {
    this._dataService.readDataFromJson()
      .subscribe(res => {
        this.sampleData = res;
        this.paginatedData = this.sampleData;
        console.log(this.sampleData);
      });
  }

  ngOnInit() {

  }
  /**
      * By default all the rows are displayed. On clicking number of rows this method
      * is called. It will displayed only number of rows selected by user in each pagination
      */

  changeLengthOfData(option: string) {
    this.paginatedData = [];
    this.numbersArray = [];
    this.optionSelected = (parseInt(option, 10));
    if (this.optionSelected != null) {
      for (let i = 0; i < this.optionSelected; i++) {
        this.paginatedData.push(this.sampleData[i]);
      }
    }
    if ((this.sampleData.length) % this.optionSelected !== 0) {
      this.numOfPages = (this.sampleData.length / this.optionSelected) + 1;
    } else {
      this.numOfPages = Math.round(this.sampleData.length / this.optionSelected);
    }
    for (let i = 1; i <= this.numOfPages; i++) {
      this.numbersArray.push(i);
    }
  }

  /**
    * Onclick pagination, the set of user selected rows selected by user will be displayed in each page.
    */

  clickPagination(paginatedValue: number) {
    this.paginatedData = [];
    const lowerLimit = (this.optionSelected * (paginatedValue - 1));
    let upperLimit = (this.optionSelected * (paginatedValue));
    if (this.sampleData.length < upperLimit) {
      upperLimit = this.sampleData.length;
    }
    for (let i = lowerLimit; i < upperLimit; i++) {
      this.paginatedData.push(this.sampleData[i]);
    }
  }

  /**
    * This method will route to the Credit component
    */

  gotoCreditPage() {
    this.router.navigateByUrl('credit');
  }
  submitToServer(id: number, status: String) {
    const data = {id, status};
   return this.http.post('/api/submit', data).pipe(map((res: Response) => {
     console.log(id, status);
   }));
  }
}
