import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginationComponent } from './pagination.component';
import { KeysPipe } from '../keys.pipe';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Router } from '@angular/router';
// describe('Pipe: keys', () => {
//   let pipe: KeysPipe;

//   beforeEach(() => {
//     pipe = new KeysPipe();
//   });
// });

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginationComponent, KeysPipe],
      providers: [HttpClient, HttpHandler, {
        provide: Router,
        useClass: class { navigate = jasmine.createSpy('navigate'); }
    }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
