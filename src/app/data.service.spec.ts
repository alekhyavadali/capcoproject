import { TestBed, inject } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { DataService } from './data.service';

describe('DataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataService, HttpClient, HttpHandler]
    });
  });

  it('should be created', inject([DataService], (service: DataService, _http: HttpClient) => {
    expect(service).toBeTruthy();
  }));
});
