import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// Components
import { AppComponent } from './app.component';
import { DataService } from './data.service';
import {PaginationComponent} from './pagination/pagination.component';
// HTTP
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

// Routes
import { RouterModule, Routes } from '@angular/router';
import {KeysPipe} from './keys.pipe';
import { CreditComponent } from './credit/credit.component';
const appRoutes: Routes = [
  { path: '' , component: PaginationComponent},
  { path: 'credit', component: CreditComponent },
 ];
@NgModule({
  declarations: [
    AppComponent,
    PaginationComponent,
    CreditComponent,
    KeysPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [RouterModule],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
